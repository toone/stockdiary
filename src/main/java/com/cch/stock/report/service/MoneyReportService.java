package com.cch.stock.report.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cch.platform.service.BaseService;
import com.cch.platform.util.DateUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.bill.bean.SdBill;

@Service
public class MoneyReportService extends BaseService<SdBill> {
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map getData(Map<String, Object> param){
		String hql;
		 
		//1、确定日期范围     from <= (?) < to
		Calendar from=DateUtil.parseCalendar((String)param.get("dateFrom"));
		Calendar to=DateUtil.parseCalendar((String)param.get("dateTo"));
		if(to==null)to=Calendar.getInstance();
		if(from==null){
			hql="select min(billDate) from SdBill where userId=? and billBigcatory='BSTRANS'";
			List ft=this.beanDao.find(hql,WebUtil.getUser().getUserId());
			if(ft.get(0)==null) return null;
			from=Calendar.getInstance();
			from.setTime((Date)(ft.get(0)));
		}
		from.set(Calendar.HOUR_OF_DAY, 0);
		from.set(Calendar.SECOND,0);
		from.set(Calendar.MINUTE,0);
		from.set(Calendar.MILLISECOND,0);
		to.set(Calendar.HOUR_OF_DAY, 0);
		to.set(Calendar.SECOND,0);
		to.set(Calendar.MINUTE,0);
		to.set(Calendar.MILLISECOND,0);
		to.add(Calendar.DAY_OF_MONTH, 1);
		Calendar tfrom=Calendar.getInstance();
		tfrom.setTime(from.getTime());
		
		//2、求期初金额
		double beginmny=0,inmny=0,outmny=0,endmny=0;
		hql="select sum(billMoney) from SdBill where userId=? and billDate<? and billBigcatory='BSTRANS'";
		List lt=this.beanDao.find(hql,WebUtil.getUser().getUserId(),from.getTime());
		if(lt.get(0)!=null) beginmny=(Double)(lt.get(0));
		
		//3、查询银证转账详细数据
		hql="from SdBill where userId=? and billBigcatory='BSTRANS' and billDate>=? and billDate<? order by billDate asc";
		List<SdBill> lst=this.beanDao.find(hql,WebUtil.getUser().getUserId(),from.getTime(),to.getTime());

		for(SdBill billone:lst){
			if ("BSINT".equals(billone.getBillCatory())) {
				inmny += billone.getBillMoney();
			} else {
				outmny -= billone.getBillMoney();
			}
		}
		endmny=beginmny+inmny-outmny;
		
		List data=new ArrayList();
		int j=0;
		double tmny=beginmny;
		for(;tfrom.compareTo(to)<0;tfrom.add(Calendar.DAY_OF_MONTH, 1)){
			if(!DateUtil.isOpen(tfrom)) continue;
			for(;j<lst.size();j++){
				SdBill billone=lst.get(j);
				Calendar c=Calendar.getInstance();
				c.setTime(billone.getBillDate());
				c.add(Calendar.DAY_OF_MONTH, -1);
				if(tfrom.compareTo(c)>0)
					tmny+=billone.getBillMoney();
				else break;
			}
			List data1=new ArrayList();
			data1.add(tfrom.getTimeInMillis());
			data1.add(tmny);
			data.add(data1);
		}
		
		//4、组装返回值
		Map result=new HashMap();
		result.put("dateFrom", from);
		to.add(Calendar.DAY_OF_MONTH, -1);
		result.put("dateTo", to);
		result.put("inmny", inmny);
		result.put("beginmny", beginmny);
		result.put("inmny", inmny);
		result.put("outmny", outmny);
		result.put("endmny", endmny);
		result.put("data", data);
		return result;
	}
	
}