package com.cch.platform.core.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * CorePreference entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "core_preference")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CorePreference implements java.io.Serializable {

	// Fields

	private String prefCode;
	private String prefName;
	private String prefType;
	private String defaultValue;

	// Constructors

	/** default constructor */
	public CorePreference() {
	}

	/** minimal constructor */
	public CorePreference(String prefCode) {
		this.prefCode = prefCode;
	}

	/** full constructor */
	public CorePreference(String prefCode, String prefName, String prefType,
			String defaultValue) {
		this.prefCode = prefCode;
		this.prefName = prefName;
		this.prefType = prefType;
		this.defaultValue = defaultValue;
	}

	// Property accessors
	@Id
	@Column(name = "pref_code", unique = true, nullable = false, length = 100)
	public String getPrefCode() {
		return this.prefCode;
	}

	public void setPrefCode(String prefCode) {
		this.prefCode = prefCode;
	}

	@Column(name = "pref_name", length = 100)
	public String getPrefName() {
		return this.prefName;
	}

	public void setPrefName(String prefName) {
		this.prefName = prefName;
	}

	@Column(name = "pref_type", length = 100)
	public String getPrefType() {
		return this.prefType;
	}

	public void setPrefType(String prefType) {
		this.prefType = prefType;
	}

	@Column(name = "default_value", length = 500)
	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}