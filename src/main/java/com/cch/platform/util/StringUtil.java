package com.cch.platform.util;

public class StringUtil {
	
	public static String ob2Str(Object value){
    	String str=null;
    	if(value!=null){
    		str=value.toString();
    	}
    	return str;
    }
	
	public static String ob2Str(Object value, String de){
		if(isEmpty(value)){
			return de;
		}
    	return value.toString();
    }
	
    public static boolean isEmpty(Object value){
    	String str=ob2Str(value);
        return str == null || "".equals(str.trim());
    }
    
    public static String null2blank(String value){
        return value != null ? value : "";
    }
    
    public static int parseInt(Object value,int de){
    	String str=ob2Str(value);
    	if(str == null || "".equals(str.trim())){
    		return de;
    	}else{ 
    		return Integer.parseInt(str);
    	}
    }
    
    public static double parseDouble(String value){
    	if(isEmpty(value)){
    		return 0;
    	}else{ 
    		return Double.parseDouble(value);
    	}
    }
    
    public static String[] split2Array(String string, String regex){
        if(isEmpty(string)) return new String[0];
        else return string.split(regex);
    }
    
	public static String getMD5(String source) {
		String s = null;
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			md.update(source.getBytes());
			byte tmp[] = md.digest();    //MD5 的计算结果是一个 128 位的长整数，
			char str[] = new char[16 * 2];
			int k = 0;
			for (int i = 0; i < 16; i++) {
				byte byte0 = tmp[i];
				str[k++] = HEXDIGITS[byte0 >>> 4 & 0xf]; 
				str[k++] = HEXDIGITS[byte0 & 0xf];
			}
			s = new String(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}
    // 用来将字节转换成 16 进制表示的字符
	public static char HEXDIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd','e', 'f' };
	
}
