package com.cch.platform.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static Timestamp getTimestamp(){
		Date d=new Date();
		Timestamp ts=new Timestamp(d.getTime());
		return ts;
	}
	
	public static Date parseDate(String str) {
		if(StringUtil.isEmpty(str))return null;
		Date re=null;
		try {
			Long time=Long.parseLong(str);
			re=new Date(time);
		} catch (Exception e) {
			re=null;
		}
		return re;
	}
	
	public static Calendar parseCalendar(Date date) {
		if(date==null) return null;
		Calendar re=Calendar.getInstance();
		re.setTime(date);
		return re;
	}
	
	public static Calendar parseCalendar(String str) {
		Date date=DateUtil.parseDate(str);
		return DateUtil.parseCalendar(date);
	}
	
	
	/**
	 * 检查当前日期是否开市，暂时不考虑节假日
	 * @param cal
	 * @return
	 */
	public static boolean isOpen(Calendar cal){
		int week=cal.get(Calendar.DAY_OF_WEEK);
		if(week==1||week==7) return false;
		return true;
	}
	
	public static boolean isOpen(Date date){
		Calendar cal=DateUtil.parseCalendar(date);
		return DateUtil.isOpen(cal);
	}
	
	/**
	 * 获取开始日期
	 * @param date 起始日期
	 * @param isForword 控制查找方向
	 * @return
	 */
	public static Date getOpenDay(Date date,boolean isForword){
		Calendar cal=DateUtil.parseCalendar(date);
		int step=isForword?1:-1;
		while(!isOpen(cal)){
			cal.add(Calendar.DATE, step);
		}
		return cal.getTime();
	}
	
	/**
	 * 去掉date的时间部分
	 */
	public static Date floorTime(Date date){
		Calendar ca=Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 0);
		ca.set(Calendar.SECOND,0);
		ca.set(Calendar.MINUTE,0);
		ca.set(Calendar.MILLISECOND,0);
		return ca.getTime();
	}
	
	/**
	 * 将时间部分设置为一天中最大
	 */
	public static Date ceilTime(Date date){
		Calendar ca=Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 23);
		ca.set(Calendar.SECOND,59);
		ca.set(Calendar.MINUTE,59);
		ca.set(Calendar.MILLISECOND,999);
		return ca.getTime();
	}
	
	
//	private static SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//	private static SimpleDateFormat format2=new SimpleDateFormat("yyyy-MM-dd");
//	
//	public static String getSysdate() {
//		return format2.format(new Date());
//	}
//
//	public static String getSysdatetime() {
//		return format1.format(new Date());
//	}
//	
//	public static String getDate(Date date) {
//		return format2.format(date);
//	}
//
//	public static String getDatetime(Date date) {
//		return format1.format(date);
//	}
	
}
